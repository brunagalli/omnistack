const axios = require('axios');
const Dev = require('../models/Dev')

module.exports = {

  async index(req, res) {
    const { user } = req.headers;

    const loggedDev = await Dev.findById(user);


    // tem a função de mostrar os usuarios que: não seja o mesmo que esteja logado, que não tenha recebido um like e deslike.
    const users = Dev.find({
      // and para cumprir todas as condições e não uma OU outra.
      $and: [
        { _id: { $ne: user } },
        { _id: { $nin: loggedDev.likes } },
        { _id: { $nin: loggedDev.dislikes } },

      ]
    })

    return res.json(users);
  },

  async store(req, res) {
    const { username } = req.body;

    const userExists = await Dev.findOne({ user: username });

    if(userExists) {
      return res.json(userExists)
    }

    const response = await axios.get(`https://api.github.com/users/${username}`);

    const { name, bio, avatar_url: avatar } = response.data;


    const dev = await Dev.create({
      name,
      user: username,
      bio,
      avatar
    })

    

    return res.json(dev);
  }
}
// o controller deve ter os 5 métodos fundamentais e não passar disso por questões de boas práticas na criação de api e do padrão MVC
// Index(para fazer uma listade um recurso),
//Show(para retornar um único item daquele recurso),
// Store(método de criação),
//Update e 
//Delete.