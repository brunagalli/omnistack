const Dev = require('../models/Dev');

module.exports = {
  async store(req, res) {

    const { user } = req.headers; //desestruturação do req.headers.user para ter acesso ao user
    const { devId } = req.params; // desestruturação do req.params.devId para pegar o devId

    const loggedDev = await Dev.findById(user); //método mongoose de encontrar um usuario por Id
    const targetDev = await Dev.findById(devId); // aqui pega o id do usuario que é o alvo que recebeu o like
    if (!targetDev) {
      // tratamento para veriicar se a busca não encontrou um dev
      return res.status(400).json({ error: 'Dev not exists' });
    }

    if (targetDev.likes.includes(loggedDev._id)) {
      //se o alvo do like também deu like no usuario logado então temos um match
      console.log('Deu Match')
    }

    loggedDev.likes.push(targetDev._id); //adicionado dentro de likes do usuario logado o Id do novo usuario que recebeu o like do loggedDev.

    await loggedDev.save() // salva as informações

    return res.json(loggedDev); //verifica se tá retornando corretamente
  }
}


//req.body 
//req.headers
//req.params
//req.query